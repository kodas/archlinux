<div align="center">
    <h1 align="center">Installing Arch Linux with full-disk encryption</h1>
</div>
<div align="center">
  <img src="https://img.shields.io/badge/MAINTAINED-YES-green?style=for-the-badge">
  <img src="https://img.shields.io/badge/LICENSE-Apache_2.0-blue?style=for-the-badge">
</div>
<div align="center">
    <h5 align="center">"This is a guid on how to install arch linux with full disk encryption"</h1>
</div>

## Installation Steps
1. Choose a keyboard layout :
    ```
    loadkeys ( keyboard layout )
    ```
2. Connect to wifi ( Only if you use wifi primarily on your system ) :
    ```
    iwctl station ( network interface ) connect ( SSID ) password ( password )
    ```
3. Enable Network Time Protocol
    ```
    timedatectl set-ntp true
    ```
4. Partition disks ( Use lsblk if you don't know what your disk is called ) :
    ```
    cfdisk /dev/( Disk name )
      1. Select gpt then enter
      2. Delete existing partitions
      3. Create a partition with 256M of storage and change the type to "EFI System"
      4. Create a second partition with 512M of storage
      5. Create a third partition using the amount of storage given to you by default ( Which will be the rest of your storage )
      6. Press write, type yes then enter and press quit
    ```
5. Format partitions
    ```
    mkfs.vfat -n "EFI System" /dev/( Disk name ) ( 1 for hhd's and ssd's. p1 for nvmes )
    mkfs.ext4 -L boot /dev/( Disk name ) 2
    mkfs.ext4 -L root /dev/ ( Disk name ) 3
    ```
6. Encryption support
    ```
    modprobe dm-crypt
    modprobe dm-mod
    ```
7. Encryption setup
    ```
    cryptsetup luksFormat -v -s 512 -h sha512 /dev/( Disk name ) 3
    cryptsetup open /dev/( Disk name ) 3 archlinux
    ```
8. Format encrypted partitions
    ```
    mkfs.btrfs -L root /dev/mapper/archlinux
9. Mounting & setting up the btrfs filesystem

    1. Mounting
        ```
        mount -t btrfs /dev/mapper/archlinux /mnt
        ```
    2. Creating subvolumes
        ```
        cd /mnt
        btrfs subvolume create @
        btrfs subvolume create @home
        btrfs subvolume create @swap
        ```
     3. Mounting subvolumes
        ```
        cd /
        umount -R /mnt
        mount -t btrfs -o subvol=@ /dev/mapper/archlinux /mnt
        mkdir /mnt/home
        mount -t btrfs -o subvol=@home /dev/mapper/archlinux /mnt/home
        mkdir /mnt/swap
        mount -t btrfs -o subvol=@swap /dev/mapper/archlinux /mnt/swap
        ```
10. Setting up GRUB
    ``` 
    mkdir /mnt/boot
    mount /dev/( Disk Name ) 2 /mnt/boot
    mkdir /mnt/boot/efi
    mount /dev/( Disk Name ) 1 /mnt/boot/efi
    ```
11. Setting up Swap
    ``` 
    cd /mnt
    touch /mnt/swap/swapfile
    chattr +C /mnt/swap/swapfile
    fallocate /mnt/swap/swapfile -l ( Swap size: Your ram size or otherwise. Examples: 1024M, 64M, 8G, )
    chmod 0600 /mnt/swap/swapfile
    mkswap /mnt/swap/swapfile
    swapon /mnt/swap/swapfile
    ```
12. Install System
    ```
    pacstrap -i /mnt base base-devel linux linux-firmware grub efibootmgr networkmanager sudo vim
    ```
13. Genfstab
    ```
    genfstab -U /mnt > /mnt/etc/fstab
    ```
14. Chroot
    ```
    arch-chroot /mnt
    ```
15. Root password
    ```
    passwd
    ```
16. Locale
    ```
    nano /etc/locale.gen ( Scroll until you get to your locale then uncomment it. :wq to write and exit. )
    locale-gen
    echo LANG=( LANG ).UTF-8 ( Example: en_US.UTF-8 ) > /etc/locale.conf
    ```
17. Keyboard layout
    ```
    echo KEYMAP=( Keyboard layout ) ( Example: us ) > /etc/vconsole.conf
    ```
18. Timezone
    ```
    ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
    hwclock --systohc --utc
    ```
19. Hostname
    ```
    echo ( Host name of choice ) > /etc/hostname
    nano /etc/hosts ( 'Your IP' localhost.localdomain 'Your hostname of choice'. Ctrl+o to save Ctrl+x to exit. ) 
    ```
20. Grub encryption configuration
    ```
    nano /etc/default/grub ( Scroll dowm to GRUB_CMDLINE_LINUX and inside the quotes type: cryptdevice=/dev/( Disk Name ):archlinux . :wq to save and exit. )
    ```
21. Mkinitcpio configuration
    ```
    nano /etc/mkinitcpio.conf ( Scroll down to HOOKS and after block type encrypt. :wq to save and exit. )
    mkinitcpio -p linux
    ```
22. Install and configure grub
    ```
    grub-install --boot-directory=/boot --efi-directory=/boot/efi /dev/( Disk Name ) 2
    grub-mkconfig -o /boot/grub/grub.cfg
    grub-mkconfig -o /boot/efi/EFI/arch/grub.cfg
    ```
23. Close encryption
    ```
    cryptsetup close /dev/( Disk Name ) 3
    ```
23. Exit & Reboot
    ```
    exit
    reboot
    ```
24. Useradd
    Once you login as root user with the password you set earlier type :
    ```
    useradd -m -g users -G wheel -s /bin/bash ( Username of Choice )
    passwd ( Username of Choice )
    EDITOR=nano visudo ( Scroll down to %wheel All=(ALL) ALL and uncomment it. Ctrl+o to save Ctrl+x to exit. )
    ```
25. Exit
    ```
    Exit ( After exiting login with the user you created )
    ```
26. Activate Network Connection
    ```
    sudo systemctl enable NetworkManager
    sudo systemctl start NetworkManager
    (Optional: For wifi) nmcli device connect ( SSID ) password ( password )
    ```
27. Install X window system and audio
    ```
    pacman -S pulseaudio pulseaudio-alsa asla-utils xorg xorg-xinit xorg-server
    ```
### (recommended) Hardening against Evil Maid attacks
With an encrypted boot partition, nobody can see or modify your kernel image or initramfs, but you would be still vulnerable to [Evil Maid](https://www.schneier.com/blog/archives/2009/10/evil_maid_attac.html) attacks.

One possible solution is to use UEFI Secure Boot. Get rid of preloaded Secure Boot keys (you really don't want to trust Microsoft and OEM), enroll [your own Secure Boot keys](https://wiki.archlinux.org/index.php/Secure_Boot#Using_your_own_keys) and sign the GRUB boot loader with your keys. Evil Maid would be unable to boot modified boot loader (not signed by your keys) and the attack is prevented.

#### Creating keys
The following steps should be performed as the `root` user, with accompanying files stored in the `/root` directory.

##### Install `efitools`
```
pacman -S efitools
```

##### Create a GUID for owner identification
```
uuidgen --random > GUID.txt
```

##### Platform key
CN is a Common Name, which can be written as anything.

```
openssl req -newkey rsa:4096 -nodes -keyout PK.key -new -x509 -sha256 -days 3650 -subj "/CN=my Platform Key/" -out PK.crt
openssl x509 -outform DER -in PK.crt -out PK.cer
cert-to-efi-sig-list -g "$(< GUID.txt)" PK.crt PK.esl
sign-efi-sig-list -g "$(< GUID.txt)" -k PK.key -c PK.crt PK PK.esl PK.auth
```

##### Sign an empty file to allow removing Platform Key when in "User Mode"
```
sign-efi-sig-list -g "$(< GUID.txt)" -c PK.crt -k PK.key PK /dev/null rm_PK.auth
```

##### Key Exchange Key
```
openssl req -newkey rsa:4096 -nodes -keyout KEK.key -new -x509 -sha256 -days 3650 -subj "/CN=my Key Exchange Key/" -out KEK.crt
openssl x509 -outform DER -in KEK.crt -out KEK.cer
cert-to-efi-sig-list -g "$(< GUID.txt)" KEK.crt KEK.esl
sign-efi-sig-list -g "$(< GUID.txt)" -k PK.key -c PK.crt KEK KEK.esl KEK.auth
```

##### Signature Database key
```
openssl req -newkey rsa:4096 -nodes -keyout db.key -new -x509 -sha256 -days 3650 -subj "/CN=my Signature Database key/" -out db.crt
openssl x509 -outform DER -in db.crt -out db.cer
cert-to-efi-sig-list -g "$(< GUID.txt)" db.crt db.esl
sign-efi-sig-list -g "$(< GUID.txt)" -k KEK.key -c KEK.crt db db.esl db.auth
```

#### Signing bootloader and kernel
When Secure Boot is active (i.e. in "User Mode") you will only be able to launch signed binaries, so you need to sign your kernel and boot loader.

Install `sbsigntools`
```
pacman -S sbsigntools
```
```
sbsign --key db.key --cert db.crt --output /boot/vmlinuz-linux /boot/vmlinuz-linux
sbsign --key db.key --cert db.crt --output /efi/EFI/arch/grubx64.efi /efi/EFI/arch/grubx64.efi
```

##### Automatically sign bootloader and kernel on install and updates
It is necessary to sign GRUB with your UEFI Secure Boot keys every time the system is updated via `pacman`. This can be accomplished with a [pacman hook](https://jlk.fjfi.cvut.cz/arch/manpages/man/alpm-hooks.5).

Create the hooks directory
```
mkdir -p /etc/pacman.d/hooks
```

Create hooks for both the `linux` and `grub` packages

```/etc/pacman.d/hooks/99-secureboot-linux.hook```
```
[Trigger]
Operation = Install
Operation = Upgrade
Type = Package
Target = linux

[Action]
Description = Signing Kernel for SecureBoot
When = PostTransaction
Exec = /usr/bin/find /boot/ -maxdepth 1 -name 'vmlinuz-*' -exec /usr/bin/sh -c 'if ! /usr/bin/sbverify --list {} 2>/dev/null | /usr/bin/grep -q "signature certificates"; then /usr/bin/sbsign --key /root/db.key --cert /root/db.crt --output {} {}; fi' \ ;
Depends = sbsigntools
Depends = findutils
Depends = grep
```

```/etc/pacman.d/hooks/98-secureboot-grub.hook```
```
[Trigger]
Operation = Install
Operation = Upgrade
Type = Package
Target = grub

[Action]
Description = Signing GRUB for SecureBoot
When = PostTransaction
Exec = /usr/bin/find /efi/ -name 'grubx64*' -exec /usr/bin/sh -c 'if ! /usr/bin/sbverify --list {} 2>/dev/null | /usr/bin/grep -q "signature certificates"; then /usr/bin/sbsign --key /root/db.key --cert /root/db.crt --output {} {}; fi' \ ;
Depends = sbsigntools
Depends = findutils
Depends = grep
```

#### Enroll keys in firmware
##### Copy all `*.cer`, `*.esl`, `*.auth` to the EFI system partition
```
cp /root/*.cer /root/*.esl /root/*.auth /efi/
```

##### Boot into UEFI firmware setup utility (frequently but incorrectly referred to as "BIOS")
```
systemctl reboot --firmware
```

Firmwares have various different interfaces, see [Replacing Keys Using Your Firmware's Setup Utility](http://www.rodsbooks.com/efi-bootloaders/controlling-sb.html#setuputil) if the following instructions are unclear or unsuccessful.

##### Set OS Type to `Windows UEFI mode`
Find the Secure Boot options and set OS Type to `Windows UEFI mode` (yes, even if we're not on Windows.) This may be necessary for Secure Boot to function.

##### Clear preloaded Secure Boot keys


Using Key Management, clear all preloaded Secure Boot keys (Microsoft and OEM).

By clearing all Secure Boot keys, you will enter into Setup Mode (so you can enroll your own Secure Boot keys).

##### Set or append the new keys
The keys must be set in the following order:

```db => KEK => PK```

This is due to some systems exiting setup mode as soon as a `PK` is entered.

Do not load the factory defaults, instead navigate the available filesystems in search of the files previously copied to the EFI System partition.

Choose any of the formats. The firmware should prompt you to enter the type (*Note:* type names may differ slightly.)
```
*.cer is a Public Key Certificate
*.esl is a UEFI Secure Variable
*.auth is an Authenticated Variable
```

Certain firmware (such as my own) require you use the *.auth files. Try various ones until they work.

##### Set UEFI supervisor (administrator) password
You must also set your UEFI firmware supervisor (administrator) password in the Security settings, so nobody can simply boot into UEFI setup utility and turn off Secure Boot.

You should never use the same UEFI firmware supervisor password as your encryption password, because on some old laptops, the supervisor password could be recovered as plaintext from the EEPROM chip.

##### Exit and save changes
Once you've loaded all three keys and set your supervisor password, hit F10 to exit and save your changes.

If everything was done properly, your boot loader should appear on reboot.

#### Check if Secure Boot was enabled
```
od -An -t u1 /sys/firmware/efi/efivars/SecureBoot-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
```
The characters denoted by XXXX differ from machine to machine. To help with this, you can use tab completion or list the EFI variables.

If Secure Boot is enabled, this command returns 1 as the final integer in a list of five, for example:

```6  0  0  0  1```

If Secure Boot was enabled and your UEFI supervisor password set, you may now consider yourself protected against Evil Maid attacks.

28. Download a Desktop Environment or a Window Manager

    __NOTE__: I would like to point out that I have not tested all these desktops. In particular, some Login Managers may not work with a given desktop. For the                            full list of Login Managers look at the [Arch Wiki](https://wiki.archlinux.org/index.php/Display_manager) and try the one you like.

    XFCE:
        
        ```
        pacman -S xfce4 lightdm lightdm-gtk-greeter
        echo "exec startxfce4" > ~/.xinitrc
        systemctl enable lightdm
        ```
    Gnome:

        ```
        echo "exec gnome-session" > ~/.xinitrc
        sudo pacman -S gnome
        ```
    Cinnamon:

        ```
        echo "exec cinnamon-session" > ~/.xinitrc
        sudo pacman -S cinnamon mdm
        systemctl enable mdm
        ```
    Mate:

       ```
       echo "exec mate-session" > ~/.xinitrc
       sudo pacman -S mate lightdm lightdm-gtk-greeter
       systemctl enable lightdm
       ```
    Budgie:

       ```
       echo "export XDG_CURRENT_DESKTOP=Budgie:GNOME" > ~/.xinitrc
       echo "exec budgie-desktop" >> ~/.xinitrc
       sudo pacman -S budgie-desktop lightdm lightdm-gtk-greeter
       systemctl enable lightdm
       ```
    Openbox:

       ```
       echo "exec openbox-session" > ~/.xinitrc
       sudo pacman -S openbox lightdm lightdm-gtk-greeter
       systemctl enable lightdm
       ```
    i3:

       ```
       echo "exec i3"  > ~/.xinitrc
       pacman -S i3 rxvt-unicode dmenu lightdm
       systemctl enable lightdm
       ```
    Awesome:

       ```
       echo "exec awesome"  > ~/.xinitrc
       sudo pacman -S awesome lightdm
       systemctl enable lightdm
       ```
    Deepin:
    
       ```
       echo "exec startdde"  > ~/.xinitrc
       sudo pacman -S deepin lightdm
       systemctl enable lightdm
       ``` 
       Also, edit the file /etc/lightdm/lightdm.conf to have this line:
       ```
       greeter-session=lightdm-deepin-greeter
       ```
    LXDE:
       ```
       echo "exec startlxde"  > ~/.xinitrc
       sudo pacman -S lxdm-gtk3 lxdm
       ```
29. Reboot & Enjoy
     ```
     reboot
    ```
___

<h4 align="center">I hope you enjoyed the guid I made though if you have had any isssues with my guid please submit it in them 
issues section.</h4>



